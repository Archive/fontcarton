/* Fontcarton
 * Copyright (C) 2001 Bastien Nocera <hadess@hadess.net>
 *
 * fontcarton.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
 * USA.
 */ 


#include <gnome.h>
#include <eel/eel.h>
#include <eel/eel-debug-drawing.h>
#include <glade/glade.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>
#include <config.h>

#include "eel-scalable-font-extensions.h"
#include "fontcarton.h"

GladeXML *xml = NULL;
GtkWidget *main_window;
GdkPixbuf *pix;
GdkPixmap *dbuf = NULL;

const GtkTargetEntry target_table[] = {
	{"text/uri-list", 0, 0}
};

static gint sizelist[6] = {
        8,
        10,
        12,
        24,
	48,
	72,
};

void fc_error(gchar *msg)
{
	GtkWidget *error_dialog;

	error_dialog =
		gnome_message_box_new(msg,
				GNOME_MESSAGE_BOX_ERROR,
				GNOME_STOCK_BUTTON_OK, NULL);
	if (main_window != NULL)
		gnome_dialog_set_parent(GNOME_DIALOG(error_dialog),
				GTK_WINDOW(main_window));

	gnome_dialog_set_default(GNOME_DIALOG(error_dialog), GNOME_YES);

	gtk_widget_show(error_dialog);
	gnome_dialog_run(GNOME_DIALOG(error_dialog));
}

void fc_render_pixbuf(void)
{
	GtkWidget *area;
	GdkGC *gc;
	guint width, height;
	
	if (dbuf != NULL)
	{
		/* or should it be gdk_pixmap_unref ? */
		g_free(dbuf);
		dbuf = NULL;
	}

	width = gdk_pixbuf_get_width(pix);
	height = gdk_pixbuf_get_height(pix);

	area = glade_xml_get_widget(xml, "drawingarea1");

	gtk_drawing_area_size(GTK_DRAWING_AREA(area), width, height);

	dbuf = gdk_pixmap_new(main_window->window, width, height, -1);
	gc = gdk_gc_new(GTK_WIDGET(main_window)->window);
	gdk_pixbuf_render_to_drawable(pix, dbuf, gc,
			0, 0, 0, 0,
			width, height,
			GDK_RGB_DITHER_NONE, 0, 0);
}

int on_drawingarea1_expose_event(GtkWidget *area,
				 GdkEventExpose *event,
				 gpointer userdata)
{
	if (dbuf == NULL)
		return 0;

	gdk_window_set_back_pixmap (area->window, NULL, FALSE);

	gdk_window_set_background(area->window, &area->style->white);

	gdk_window_copy_area(area->window,
			area->style->fg_gc[GTK_STATE_NORMAL],
			event->area.x, event->area.y,
			dbuf,
			event->area.x, event->area.y,
			event->area.width, event->area.height);

	return 0;
}

GdkPixbuf *fc_font_to_pixbuf(gchar *font_path)
{
	GdkPixbuf *pixbuf;
	EelScalableFont *def_font, *font;
	gchar *text;
	guint width, height;
	guint text_len;
	gint i, y;

	def_font = eel_scalable_font_get_default_font ();
	g_return_val_if_fail(def_font != NULL, NULL);
	font = eel_scalable_font_new(font_path);
	if (font == NULL)
	{
		gtk_object_destroy(GTK_OBJECT(def_font));
		return NULL;
	}

	/* 8 + 10 + 12 + 24 + 48 + 72		fonts themselves
	 * + 6 * 12				fonts descriptions
	 * + 2 * 8				top and bottom margins
	 * + 15 * 4				margins between the labels
	 * + 24					font name
	 * = 346
	 */
	height = 346;

	/* Do not translate this sentence literally. It's a pangram (check
	 * google for infos), ie. it's a sentence which contains all the
	 * letters from the alphabet. I don't know if that makes any sense,
	 * or is even possible for people that don't use the latin alphabet,
	 * but if you translate it literally, it doesn't make any sense.
	 */
	text = g_strdup_printf(_("The quick brown fox jumps over the lazy dog. 0123456789"));
	text_len = strlen(text);
	/* g_print("%d\n", text_len); */
	
	width = eel_scalable_font_text_width(font, 72, text, text_len) + 8;

	pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, width, height);
	eel_debug_pixbuf_draw_rectangle (pixbuf,
			TRUE,
			-1, -1, -1, -1,
			EEL_RGB_COLOR_WHITE,
			EEL_OPACITY_FULLY_OPAQUE);

	/* we start drawing at 8 pixels on the y axis */
	y = 8;
	{
		gchar *name;
		gchar *name_and_type;

		name = g_basename(font_path);
		switch (eel_scalable_font_type(font))
		{
		case 0:
			name_and_type = g_strdup_printf(_("%s - Unknown type"),
					name);
			break;
		case EEL_FONT_POSTSCRIPT:
			name_and_type = g_strdup_printf(_("%s - Type1 font"),
					name);
			break;
		case EEL_FONT_TRUE_TYPE:
			name_and_type = g_strdup_printf(_("%s - Truetype font"),
					name);
			break;
		default:
			name_and_type = g_strdup_printf(_("%s - Unknown type"),
					name);
		}

		eel_scalable_font_draw_text (def_font,
				pixbuf,
				8, y,
				eel_gdk_pixbuf_whole_pixbuf,
				24,
				name_and_type,
				strlen(name_and_type),
				EEL_RGBA_COLOR_OPAQUE_BLACK,
				EEL_OPACITY_FULLY_OPAQUE);

		g_free(name_and_type);

		y = y + 28; /* 24 pixels for the label + 4 for the margin */
	}
	for (i = 0 ; i < 6 ; i++)
	{
		gchar *label;

		label = g_strdup_printf(_("%d pt."), sizelist[i]);
		eel_scalable_font_draw_text (def_font,
				pixbuf,
				8, y,
				eel_gdk_pixbuf_whole_pixbuf,
				12,
				label,
				strlen(label),
				EEL_RGBA_COLOR_OPAQUE_BLACK,
				EEL_OPACITY_FULLY_OPAQUE);
		g_free(label);

		y = y + 16; /* 10 pixels for the label + 4 for the margin */
		
		eel_scalable_font_draw_text (font,
				pixbuf,
				8, y,
				eel_gdk_pixbuf_whole_pixbuf,
				sizelist[i],
				text, text_len,
				EEL_RGBA_COLOR_OPAQUE_BLACK,
				EEL_OPACITY_FULLY_OPAQUE);
		y = y + sizelist[i] + 4;
	}

	gtk_object_destroy(GTK_OBJECT(font));
	gtk_object_destroy(GTK_OBJECT(def_font));
	g_free(text);

	return pixbuf;
}

gboolean fc_display_font(gchar *font_path)
{
	gchar *full_path;
	gboolean retval;

	//g_print("Display %s\n", font_path);
	g_return_val_if_fail(font_path != NULL, FALSE);
	if (font_path[0] != '/')
	{
		gchar *current_dir;

		current_dir = g_get_current_dir();
		full_path = g_strconcat(current_dir, G_DIR_SEPARATOR_S,
				font_path, NULL);
		g_free(current_dir);
	} else
		full_path = g_strdup(font_path);

	if (eel_font_manager_file_is_scalable_font(full_path) == TRUE)
	{
		retval = TRUE;
		pix = fc_font_to_pixbuf(full_path);
		fc_render_pixbuf();
		gdk_pixbuf_unref(pix);
	} else {
		gchar *msg;

		retval = FALSE;
		msg = g_strdup_printf(_("%s is not a font, or is not supported by Font Carton\nTry another font"), g_basename(full_path));
		
		fc_error(msg);
		g_free(msg);
	}
	g_free(full_path);

	return retval;
}
#if 0
void fc_show_prop(void)
{
	fc_error("Not implemented yet");
}
#endif
void fc_show_about(void)
{
	GtkWidget *about;

	about = glade_xml_get_widget(xml, "about");
	gnome_dialog_set_parent(GNOME_DIALOG(about),
			GTK_WINDOW(main_window));
	gtk_widget_show(about);
}

void fc_hide_about(void)
{
	GtkWidget *about;

	about = glade_xml_get_widget(xml, "about");
	gtk_widget_hide(about);
}
#if 0
void fc_load(void)
{
	fc_error("Not implemented yet");
}
#endif
void fc_exit(void)
{
	guint height, width;
	
	gdk_window_get_size(main_window->window,
			&width,
			&height);
	gnome_config_push_prefix("/fontcarton/General/");
	gnome_config_set_int("width", width);
	gnome_config_set_int("height", height);
	gnome_config_sync();
	gnome_config_pop_prefix();
	exit(0);
}

/* Signal handlers here */

void
on_drawingarea1_drag_data_received(GtkWidget * widget,
			      GdkDragContext * drag_context,
			      gint x,
			      gint y,
			      GtkSelectionData * data,
			      guint info, guint time, gpointer user_data)
{
	GList *files;
	gchar *uri, *filename;

	gtk_drag_finish(drag_context, TRUE, FALSE, time);
	files = gnome_uri_list_extract_uris(data->data);

	if (files == NULL)
	{
		fc_error(_("An error happened while getting the data from the drag'n'drop\nContact the author of Fontcarton with a complete bug report."));
		return;
	}

	uri = files->data;
	filename = gnome_vfs_get_local_path_from_uri(uri);
	fc_display_font(filename);
	g_free(filename);
	gnome_vfs_list_deep_free(files);
}

void on_about_close (GtkWidget * widget,
		     GdkEvent * event, gpointer user_data)
{
	fc_hide_about();
}

void on_about_clicked(GtkButton * button, gpointer user_data)
{
	fc_hide_about();
}

void on_about1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	fc_show_about();
}
#if 0
void on_button3_clicked(GtkButton * button, gpointer user_data)
{
	fc_load();
}

void on_button1_clicked(GtkButton * button, gpointer user_data)
{
	fc_show_prop();
}

void on_button2_clicked(GtkButton * button, gpointer user_data)
{
	fc_show_about();
}

void on_open1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	fc_load();
}

void on_properties1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	fc_show_prop();
}
#endif
void on_exit1_activate(GtkMenuItem * menuitem, gpointer user_data)
{
	fc_exit();
}

gboolean on_fontcarton_destroy_event(GtkWidget * widget,
				     GdkEvent * event, gpointer user_data)
{
	fc_exit();
	return TRUE;
}

int
main (int argc, char *argv[])
{

#ifdef ENABLE_NLS
	bindtextdomain(PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain(PACKAGE);
#endif

	gnome_init ("fontcarton", VERSION, argc, argv);
	gdk_rgb_init ();
	glade_gnome_init();

	/* load the glade file */
	{
		gchar *glade_path;

		glade_path = g_strconcat(FC_DATA, "fontcarton.glade", NULL);

		xml = glade_xml_new (glade_path, NULL);

		if (xml == NULL)
		{
			fc_error(_("Couldn't load the Glade file.\nMake sure that Fontcarton is properly installed."));
			exit(1);
		}

		glade_xml_signal_autoconnect(xml);

		g_free(glade_path);
	}

	/* setup the drop zone */
	{
		GtkWidget *drawing;

		drawing = glade_xml_get_widget(xml, "drawingarea1");
		gtk_drag_dest_set(drawing,
				GTK_DEST_DEFAULT_ALL,
				target_table, 1, GDK_ACTION_COPY);
	}

	/* to render the images, the main window needs to be realised */
	main_window = glade_xml_get_widget(xml, "fontcarton");

	/* load up the window sizes */
	{
		gnome_config_push_prefix("/fontcarton/General/");

		gtk_widget_set_usize(main_window,
				gnome_config_get_int("width=450"),
				gnome_config_get_int("height=300"));

		gnome_config_pop_prefix();
		gnome_app_enable_layout_config(GNOME_APP(main_window),
				TRUE);
	}
	gtk_widget_show(main_window);

	/* Change the color of the background, the window needs to be
	 * realised for that */
	{
		GtkWidget *drawing;

		drawing = glade_xml_get_widget(xml, "drawingarea1");
		gdk_window_set_background(drawing->window,
				&drawing->style->white);
	}

	if (argc > 1)
	{
		if (!fc_display_font(argv[1]))
			exit(1);
	}

	gtk_main();

	return 0;
}
