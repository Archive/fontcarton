
#ifndef EEL_SCALABLE_FONT_EXTENSIONS_H
#define EEL_SCALABLE_FONT_EXTENSIONS_H

#include <glib.h>
#include <gtk/gtkobject.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <eel/eel-font-manager.h>
#include <eel/eel-string-list.h>
#include <eel/eel-art-extensions.h>
#include <libart_lgpl/art_rect.h>
#include <librsvg/rsvg.h>
#include <librsvg/rsvg-ft.h>

#define POSTSCRIPT_FONT_MIME_TYPE       "application/x-font-type1"
#define TRUE_TYPE_FONT_MIME_TYPE        "application/x-font-ttf"

/* Detail member struct from eel-scalable-font.c */
struct EelScalableFontDetails
{
	RsvgFTFontHandle font_handle;
	char *font_file_name;
};

EelFontType eel_scalable_font_type (EelScalableFont *font);

EelFontType
eel_scalable_font_type (EelScalableFont *font)
{
	char *uri;
	GnomeVFSFileInfo *info;
	GnomeVFSResult result;
	gint font_type = 0;

	g_return_val_if_fail (eel_strlen (font->details->font_file_name) > 0,
			0);

	if (gnome_vfs_initialized () == FALSE) {
		gnome_vfs_init ();
	}

	uri = gnome_vfs_get_uri_from_local_path (font->details->font_file_name);

	info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info (uri, info,
			GNOME_VFS_FILE_INFO_GET_MIME_TYPE
			| GNOME_VFS_FILE_INFO_FOLLOW_LINKS);

	if (result == GNOME_VFS_OK) {
		if (eel_istr_is_equal (info->mime_type, POSTSCRIPT_FONT_MIME_TYPE)) {
			font_type = EEL_FONT_POSTSCRIPT;
		} else if (eel_istr_is_equal (info->mime_type, TRUE_TYPE_FONT_MIME_TYPE)) {
			font_type = EEL_FONT_TRUE_TYPE;
		}
	}

	gnome_vfs_file_info_unref (info);
	g_free (uri);

	return font_type;
}

#endif
