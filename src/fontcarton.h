void fc_error(gchar *msg);
void fc_render_pixbuf(void);
int on_drawingarea1_expose_event(GtkWidget *area,
                                 GdkEventExpose *event,
                                 gpointer userdata);
GdkPixbuf *fc_font_to_pixbuf(gchar *font_path);
gboolean fc_display_font(gchar *font_path);
void fc_show_about(void);
void fc_hide_about(void);
void fc_exit(void);
void on_drawingarea1_drag_data_received(GtkWidget * widget,
                              GdkDragContext * drag_context,
                              gint x,
                              gint y,
                              GtkSelectionData * data,
                              guint info, guint time, gpointer user_data);
void on_about_close (GtkWidget * widget,
                     GdkEvent * event, gpointer user_data);
void on_about_clicked(GtkButton * button, gpointer user_data);
void on_about1_activate(GtkMenuItem * menuitem, gpointer user_data);
void on_exit1_activate(GtkMenuItem * menuitem, gpointer user_data);
gboolean on_fontcarton_destroy_event(GtkWidget * widget,
                                     GdkEvent * event, gpointer user_data);

